Description: Disable tests requiring Internet
 By default, the test suite requires a connection to the CDDB
 server. This uses an environment variable, HAS_INTERNET to
 control whether live tests are run.
Origin: vendor
Forwarded: not-needed
Author: Jonathan Yu <frequency@cpan.org>
--- a/t/01_cddb.t
+++ b/t/01_cddb.t
@@ -7,12 +7,18 @@
 
 use strict;
 use CDDB;
-use Test::More tests => 25;
+use Test::More;
 
 BEGIN {
 	select(STDOUT); $|=1;
 };
 
+if ($ENV{NO_NETWORK}) {
+  plan skip_all => 'Unset NO_NETWORK to enable live tests';
+}
+
+plan tests => 25;
+
 my ($i, $result);
 
 ### test connecting
